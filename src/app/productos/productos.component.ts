import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ProductosService } from '../services/producto.services';
import { Producto } from '../models/producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css'],
  providers: [ProductosService]
})
export class ProductosComponent implements OnInit {

  public titulo:string;
  public productos: Producto[];

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _productosService: ProductosService
  ) {

    this.titulo = 'Productos';
   }

  ngOnInit() {
    console.log('Se ha cargado el componente Productos');
    this.getProductos();
  }


    getProductos(){
      this._productosService.getProductos().subscribe(
        result => {
          this.productos = result.data;
        },
        error => {
          console.log(<any>error);
        });
    }

    public confirmado;
    
    borrarConfirm(id){
      this.confirmado = id;
    }

    cancelConfirm(){
      return alert("Se ha cancelado la accion")
    }

    onDeleteProducto(id){
      this._productosService.deleteProducto(id).subscribe(
        response => {
          if (response['code'] == 200) {
            this.getProductos();
          } else {
            console.log('Error al borrar el producto')
          }
        }, error => {
          console.log(<any>error)
        }
      );
    }
}