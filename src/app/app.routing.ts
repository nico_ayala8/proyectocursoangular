import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';

// Componentes 

import { HomeComponent } from './home/home.component';
import { ErrorComponent } from './error/error.component';
import { ProductosComponent } from './productos/productos.component';
import { ProductoAddComponent } from './producto-add/producto-add.component';
import { ProductoDetailComponent } from './producto-detail/producto-detail.component';

const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'home', component: HomeComponent},
    {path: 'error', component: ErrorComponent},
    {path: 'productos', component: ProductosComponent},
    {path: 'addproducto', component: ProductoAddComponent},
    {path: 'producto/:id', component: ProductoDetailComponent},
    {path: '**', component: ErrorComponent}
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);