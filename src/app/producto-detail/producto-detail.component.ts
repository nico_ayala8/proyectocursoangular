import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

// Componentes
import { ProductosService } from '../services/producto.services';
import { Producto } from "../models/producto";

@Component({
  selector: 'app-producto-detail',
  templateUrl: './producto-detail.component.html',
  styleUrls: ['./producto-detail.component.css'],
  providers: [ProductosService]
})
export class ProductoDetailComponent implements OnInit {
    public producto: Producto;
    public retorno:any

  constructor(
    private _productoService: ProductosService,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit(): void {
    console.log("Se ha cargado el componente de detalles")
    this.getProducto();
  }

  getProducto(){
    // Parametros de la URL de los productos
    this._route.params.forEach((params: Params) => {
      let id = params['id'];

      // Capturar el producto por ID
      this._productoService.getProducto(id).subscribe(
        response => {
          if (response['code'] == 200) {
            this.producto = response['data'];
            console.log(response['data']);
          } else {
            console.log("No se ha ejecutado correctamente. Error.")
          }
        }, 
          error => {
          console.log(<any>error)
          }
      );
    });
  }

}
